var app = angular.module('myApp',['ngSanitize']);

app.controller('myCtrl',function($scope,SrvImportantNews,SrvCatgeory,SrvCategoriesAndNews,SrvGetLatestNews) { 
    
    $scope.arrImportantNews = [];
    $scope.arrNewsDetails = [];
    $scope.arrCategory = [];
    $scope.arrCategoriesAndNews = [];
    $scope.arrCategoryImportantNews = [];
    $scope.arrLatestNews = [];
    $scope.arrRelatedNewsCategory = [];

    $scope.init = function(){
        
        $scope.importantNews();
        $scope.listCategory();
        $scope.categoriesAndNews();
        $scope.getLatestNews();
        $scope.relatedNewsCategory(localStorage.getItem('category_id'));

    }
    
    $scope.listCategory = function(){
        SrvCatgeory.ListCategory().then(function(response){
            if(response.data.status.code == 200){
                $scope.arrCategory = response.data.data;
            }
        })
    }

    $scope.importantNews = function(){
        SrvImportantNews.ImportantNews().then(function(response){
            if(response.data.status.code == 200){
                $scope.arrImportantNews = response.data.data;
                $scope.arrImportantNews['body'];
            }
        })
    }
   
    $scope.newsDetails = function(news_id){
       localStorage.setItem('news_id',news_id);
       window.location.href="new_detail.html";
    }

    $scope.categoriesAndNews = function(){
        SrvCategoriesAndNews.CategoriesAndNews().then(function(response){
            if(response.data.status.code == 200){
                $scope.arrCategoriesAndNews = response.data.data;
                for( var i = 0; i < $scope.arrCategoriesAndNews.length; i++){
                    $scope.arrCategoriesAndNews[i].relatedNews = $scope.arrCategoriesAndNews[i].relatedNews.reverse();
                    if( $scope.arrCategoriesAndNews[i].name === "أهم الأخبار"){
                        $scope.arrCategoryImportantNews = $scope.arrCategoriesAndNews[i];
                        $scope.arrCategoriesAndNews.splice(i, 1); 
                    }
                }
            }
        })
    }

    $scope.getLatestNews = function(){
        SrvGetLatestNews.GetLatestNews().then(function(response){
            if(response.data.status.code == 200){
                $scope.arrLatestNews = response.data.data;
            }
        })
    }

    $scope.setCategoryId = function(category_id){
        localStorage.setItem('category_id',category_id);
        window.location.href="news_category.html";
    }

    $scope.relatedNewsCategoryPagination = function(page){
        var pageNumber = page.charAt(page.length-1);
        var category_id = page.slice(55,56);
       
        SrvCategoriesAndNews.RelatedNewsCategoryPagination(category_id, pageNumber).then(function(response){
            if(response.data.status.code == 200){
                $scope.arrRelatedNewsCategory = response.data;
            }
        })
    }

    $scope.relatedNewsCategory = function(category_id){
        SrvCategoriesAndNews.RelatedNewsCategory(category_id).then(function(response){
            if(response.data.status.code == 200){
                $scope.arrRelatedNewsCategory = response.data;
            }
        })
    }
    
    $scope.init();
    
})



app.controller('myNewsDetailsCtrl',function($scope,SrvCatgeory,SrvNewsDetails) { 
    
    
    $scope.news = '';
    $scope.ads = '';
    $scope.tags = '';
    $scope.arrCategory = [];
    $scope.arrNewsAttachedtoCategory = [];
    $scope.arrNewsAttachedToTitle = [];

    $scope.init = function(){
        $scope.listCategory();
        $scope.newsDetails(localStorage.getItem('news_id'));
    }
    
    $scope.listCategory = function(){
        SrvCatgeory.ListCategory().then(function(response){
            if(response.data.status.code == 200){
                $scope.arrCategory = response.data.data;
            }
        })
    }

    $scope.setNewsId = function(news_id){
        localStorage.setItem('news_id',news_id);
        window.location.href="new_detail.html";
    }

    $scope.newsDetails = function(news_id){
        SrvNewsDetails.NewsDetails(news_id).then(function(response){
            if(response.data.status.code == 200){
                $scope.news = response.data.newsDetails[0];
                $scope.tags = response.data.newsDetails[0].tags;
                $scope.arrNewsAttachedtoCategory = response.data.relatedNews;
                $scope.arrNewsAttachedToTitle = response.data.similarNews;
                $scope.ads = response.data.newsAds[0];
            }
        })
    }

    $scope.init();
    
})



